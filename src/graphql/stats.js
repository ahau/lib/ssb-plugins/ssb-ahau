const gql = require('graphql-tag')
const si = require('systeminformation')
const fs = require('fs')
const path = require('path')

const { promisify: p } = require('util')
const deleteAhau = require('../delete-ahau')

const readdir = p(fs.readdir)
const stat = p(fs.stat)

module.exports = (ssb, externalGetters = {}) => {
  const { resolvers, gettersWithCache } = Resolvers(ssb, externalGetters)

  return {
    typeDefs,
    resolvers,
    gettersWithCache
  }
}

const typeDefs = gql`
  type FileSystemStats {
    size: Float!
    used: Float!
    available: Float!
    use: Float!
    mount: String!
  }

  type IndexesStats {
    isIndexing: Boolean
    isRebuilding: Boolean
    percentageIndexed: Float
    percentageIndexedSinceStartup: Float
  }

  type AhauDBStats {
    size: Float!
    hyperBlobStats: HyperBlobStats!
  }

  type HyperBlobStats {
    size: Float!
  }

  extend type Query {
    fileSystemStats: FileSystemStats
    ahauDBStats: AhauDBStats
    latestSequence: Int
    indexes: IndexesStats
  }

  extend type Mutation {
    deleteAhau: Boolean!
  }
`

function Resolvers (ssb, externalGetters) {
  const appPath = ssb.config.path
  const hyperBlobsPath = path.join(appPath, 'hyperBlobs')

  ssb.db.reindexEncrypted.hook((rebuild, [cb]) => {
    rebuild(err => {
      cb && cb(err)
    })
  })

  const hyperBlobStats = (cb) => {
    totalSize(hyperBlobsPath)
      .catch(err => cb(err))
      .then(size => cb(null, { size }))
  }

  const resolvers = {
    Query: {
      fileSystemStats: async () => {
        const fsSize = await si.fsSize()
        const sizes = fsSize
          .filter(fs => appPath.startsWith(fs.mount))
          .sort((a, b) => b.mount.length - a.mount.length) // longest first

        if (!sizes.length) return null
        return sizes[0]
      },
      ahauDBStats: async () => {
        return {}
      },
      latestSequence: async () => {
        const latestSequence = await p(ssb.getVectorClock)()
        return latestSequence[ssb.id]
      },
      indexes: () => {
        const status = ssb.db.getStatus().value

        // this has a bit of duplicate info, but that's to keep backwards compat when moving from db1 to db2
        return {
          percentageIndexed: status.progress * 100,
          isIndexing: status.progress !== 1,
          isRebuilding: status.progress !== 1,
          percentageIndexedSinceStartup: status.progress * 100
        }
      }
    },
    AhauDBStats: {
      size: () => totalSize(appPath),
      hyperBlobStats: () => p(hyperBlobStats)()
    },
    Mutation: {
      deleteAhau: () => p(deleteAhau)(ssb)
    }
  }

  return {
    resolvers,
    gettersWithCache: {
      hyperBlobStats
    }
  }
}

// taken from https://gitlab.com/ahau/artefact-store/-/blob/master/test/helpers/util.js
async function * walk (baseDir) {
  if (baseDir[baseDir.length - 1] !== path.sep) baseDir = baseDir + path.sep
  yield * getFiles(baseDir)

  async function * getFiles (directory) {
    const files = await readdir(directory, { withFileTypes: true })

    for await (const f of files) {
      const fullPath = path.join(directory, f.name)
      if (f.isFile()) yield fullPath
      if (f.isDirectory()) yield * getFiles(fullPath)
    }
  }
}

// taken from https://gitlab.com/ahau/artefact-store/-/blob/master/test/helpers/util.js
async function totalSize (pathToWalk) {
  let totalSize = 0
  for await (const filePath of walk(pathToWalk)) {
    const { size } = await stat(filePath)
      // added this to handle errors
      .catch(err => {
        console.error(`totalSize could not stat ${filePath}:`, err)
        return { size: 0 }
      })
    totalSize += size
  }
  return totalSize
}
