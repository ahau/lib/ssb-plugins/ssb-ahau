const PERSONAL_TYPE = '__personal__'

module.exports = function FindPersonalGroup (ssb) {
  let personalGroupId

  return function findPersonalGroup (cb) {
    if (personalGroupId) return cb(null, personalGroupId)

    ssb.tribes.findByFeedId(ssb.id, (err, groups) => {
      if (err) return cb(err)

      const personalGroups = groups.filter(group => {
        return group.states[0].state.name === PERSONAL_TYPE
      })

      if (personalGroups.length === 0) return cb(new Error('no personal group found'))

      if (personalGroups.length > 1) {
        throw new Error('ssb-ahau found multiple personal group, you are only allowed one')
      }
      personalGroupId = personalGroups[0].groupId
      return cb(null, personalGroupId)
    })
  }
}
