const pull = require('pull-stream')
const paraMap = require('pull-paramap')

const FindNonPersonalTribeIds = require('./find-non-personal-tribe-ids')

module.exports = function FindAuthoredTribes (ssb) {
  const findNonPersonalTribeIds = FindNonPersonalTribeIds(ssb)

  function getGroupAuthor (data, cb) {
    ssb.get(data.root, (err, val) => {
      if (err) return cb(err)

      cb(null, { author: val.author, groupId: data.groupId })
    })
  }

  return function findAuthoredTribeIds (cb) {
    findNonPersonalTribeIds((err, tribeIds) => {
      if (err) return cb(err)

      pull(
        pull.values(tribeIds),
        paraMap(ssb.tribes.get, 5),
        paraMap(getGroupAuthor, 5), // get the author of the tribe
        pull.filter(m => m.author === ssb.id),
        pull.map(m => m.groupId),
        pull.collect(cb)
      )
    })
  }
}
