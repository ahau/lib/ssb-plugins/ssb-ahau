const test = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')

const handleErr = (err) => {
  // console.log(JSON.stringify(err, null, 2))
  return { errors: [err] }
}

function SavePerson (apollo, t) {
  return async function (input) {
    input.type = 'person'
    const res = await apollo.mutate({
      mutation: `mutation($input: PersonProfileInput!) {
        savePerson(input: $input)
      }`,
      variables: { input }
    })

    t.error(res.errors, 'save person without error')

    return res.data.savePerson
  }
}

function GetProfile (apollo, t) {
  return async function getProfile (id) {
    const res = await apollo.query({
      query: `query($id: String!) {
        profile(id: $id) {
          ...on Person {
            tiaki {
              id
              preferredName
            }
          }
          ...on Community {
            kaitiaki {
              feedId
              profile {
                id
              }
            }
          }
          authors {
            active
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
              originalAuthor
            }
          }
        }
      }`,
      variables: {
        id
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'get profile without error')

    return res.data.profile
  }
}

test('profile (person) - authors', async t => {
  t.plan(15)

  const { ssb, apollo } = await TestBot()
  const savePerson = SavePerson(apollo, t)
  const getProfile = GetProfile(apollo, t)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const profileId = await savePerson(
    {
      authors: {
        add: [feedId],
        remove: []
      },
      allowPublic: true
    }
  )
    .catch(handleErr)

  const profile = await getProfile(profileId)

  // t.true(profile.canEdit, 'returns true for canEdit')

  t.deepEqual(
    profile.tiaki,
    [
      {
        id: publicProfileId,
        preferredName: null
      }
    ],
    'returns correct tiaki'
  )

  t.deepEqual(
    profile.authors,
    [
      {
        feedId,
        active: true,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null,
          originalAuthor: feedId
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors
  const saveRes2 = await savePerson(
    {
      id: profileId, // update
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      },
      allowPublic: true
    }
  )

  t.error(saveRes2.errors, 'updates public person/profile authors without errors')

  const person = await getProfile(profileId)
  // t.true(person.canEdit, 'can still edit because *')

  // HACK: cant test dates?
  const { start } = person.authors[1].intervals[0]

  t.deepEqual(
    person.authors,
    [
      {
        feedId,
        active: false,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null,
          originalAuthor: feedId
        }
      },
      {
        feedId: '*',
        active: true,
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId again
  await savePerson({
    id: profileId,
    authors: {
      remove: [feedId] // the feedId should be ignored because its already removed
    },
    allowPublic: true
  })

  const person2 = await getProfile(profileId)

  // HACK: cant test dates?
  const { end } = person2.authors[1].intervals[0]

  // t.true(person2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    person2.authors,
    [
      {
        feedId,
        active: false,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null,
          originalAuthor: feedId
        }
      },
      {
        feedId: '*',
        active: true,
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  // update our publicProfileId
  // get the record again
  // should see preferredName changed!
  await savePerson({
    id: publicProfileId,
    preferredName: 'titania',
    allowPublic: true
  })
  const person3 = await getProfile(profileId)

  t.deepEqual(
    person3.authors,
    [
      {
        feedId,
        active: false,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: 'titania', // << update reflected here!
          originalAuthor: feedId
        }
      },
      {
        feedId: '*',
        active: true,
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})

test('profile (community) - authors', async t => {
  const { ssb, apollo } = await TestBot()

  const getProfile = GetProfile(apollo, t)

  const communityProfile = {
    preferredName: 'Plant Collectors'
  }

  const groupRes = await apollo.mutate({
    mutation: `
      mutation ($communityProfile: CommunityProfileInput) {
        initGroup(communityProfile: $communityProfile) {
          groupId
        }
      }
    `,
    variables: {
      communityProfile
    }
  })
    .catch(handleErr)

  const groupId = groupRes.data.initGroup.groupId

  const groupProfiles = await p(ssb.profile.findByGroupId)(groupId)
  const privateProfileId = groupProfiles.private[0].key

  const profile = await getProfile(privateProfileId)
  const profiles = await p(ssb.profile.findByFeedId)(ssb.id, { groupId })

  t.deepEqual(
    profile,
    {
      kaitiaki: [
        {
          feedId: ssb.id,
          profile: {
            id: profiles.private[0].key
          }
        }
      ],
      authors: [
        {
          feedId: ssb.id,
          active: true,
          intervals: [{ start: 21, end: null }],
          profile: {
            id: profiles.private[0].key,
            preferredName: null,
            originalAuthor: ssb.id
          }
        }
      ]
    },
    'returns matching community profile'
  )

  ssb.close()
  t.end()
})

test('profile (community) - ALL_AUTHORS', async t => {
  const { ssb, apollo } = await TestBot()

  const savePerson = SavePerson(apollo, t)
  const getProfile = GetProfile(apollo, t)

  // create the group
  const communityProfile = {
    preferredName: 'Plant Collectors'
  }

  const groupRes = await apollo.mutate({
    mutation: `
      mutation ($communityProfile: CommunityProfileInput) {
        initGroup(communityProfile: $communityProfile) {
          groupId
        }
      }
    `,
    variables: {
      communityProfile
    }
  })
    .catch(handleErr)

  const groupId = groupRes.data.initGroup.groupId

  const groupProfiles = await p(ssb.profile.findByGroupId)(groupId)
  const privateProfileId = groupProfiles.private[0].key

  const profile = await getProfile(privateProfileId)
  const profiles = await p(ssb.profile.findByFeedId)(ssb.id, { groupId })

  t.deepEqual(
    profile,
    {
      kaitiaki: [
        {
          feedId: ssb.id,
          profile: {
            id: profiles.private[0].key
          }
        }
      ],
      authors: [
        {
          active: true,
          feedId: ssb.id,
          intervals: [{
            start: profile.authors[0].intervals[0].start, // hack because the start is a date
            end: null
          }],
          profile: {
            id: profiles.private[0].key,
            originalAuthor: ssb.id,
            preferredName: null
          }
        }
      ]
    },
    'initial authors is the creator'
  )

  // remove ssb.id and add all authors
  const updateRes = await savePerson({
    id: privateProfileId,
    authors: {
      add: ['*'],
      remove: [ssb.id]
    }
  })

  t.error(updateRes.errors, 'remove ssb.id authors and add all authors')

  // get the updated profile
  const profileUpdate = await getProfile(privateProfileId)

  t.deepEqual(
    profileUpdate,
    {
      kaitiaki: [],
      authors: [
        {
          feedId: ssb.id,
          active: false,
          intervals: [{ start: 21, end: 23 }],
          profile: {
            id: profiles.private[0].key,
            preferredName: null,
            originalAuthor: ssb.id
          }
        },
        {
          feedId: '*',
          active: true,
          intervals: [{
            start: profileUpdate.authors[1].intervals[0].start,
            end: null
          }],
          profile: null
        }
      ]
    },
    'ssb.id as an author was disabled and all authors is enabled'
  )

  ssb.close()
  t.end()
})
