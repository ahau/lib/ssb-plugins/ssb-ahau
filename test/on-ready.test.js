const test = require('tape')
const TestBot = require('./test-bot')

test('onReady', async t => {
  // start the server
  const { ssb } = await TestBot()

  ssb.ahau.onReady(() => {
    t.pass('ahau.onReady!')

    // this following bit seem really slow...??
    ssb.close()
    t.end()
  })
})
