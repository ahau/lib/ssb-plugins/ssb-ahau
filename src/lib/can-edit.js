const isActiveAuthor = require('./is-active-author')

const ALL_AUTHORS = '*'

module.exports = function CanEdit (ssb) {
  return function canEdit (feedId, authors, cb) {
    if (!authors) return cb(new Error('the profile returned is missing authors'))

    if (isActiveAuthor(authors[ALL_AUTHORS], Date.now())) return cb(null, true)

    if (feedId === ALL_AUTHORS) return cb(null, false)

    ssb.getFeedState(feedId, (err, state) => {
      if (err) return cb(err)

      cb(null, isActiveAuthor(authors[feedId], state.sequence))
    })
  }
}
