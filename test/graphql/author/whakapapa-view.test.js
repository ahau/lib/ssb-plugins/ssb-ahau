const test = require('tape')
const TestBot = require('../../test-bot')

function Save (apollo) {
  return async function save (input) {
    return apollo.mutate({
      mutation: `mutation($input: WhakapapaViewInput!) {
        saveWhakapapaView(input: $input)
      }`,
      variables: { input }
    })
  }
}

function Get (apollo) {
  return async function get (id) {
    return apollo.query({
      query: `query($id: String!) {
        whakapapaView(id: $id) {
          canEdit
          tiaki {
            id
            preferredName
          }
          authors {
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
            }
          }
        }
      }`,
      variables: {
        id
      }
    })
  }
}

test('whakapapa - authors', async t => {
  t.plan(13)

  const { ssb, apollo } = await TestBot({ recpsGuard: true })

  const save = Save(apollo)
  const get = Get(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const saveWhakapapaRes = await save(
    {
      name: 'Whanau',
      authors: {
        add: [feedId],
        remove: []
      },
      recps: [feedId]
    }
  )

  t.error(saveWhakapapaRes.errors, 'creates whakapapa without errors')
  const whakapapaId = saveWhakapapaRes.data.saveWhakapapaView

  const whakapapaRes = await get(whakapapaId)

  t.error(whakapapaRes.errors, 'gets whakapapa without errors')

  const whakapapaView = whakapapaRes.data.whakapapaView
  t.true(whakapapaView.canEdit, 'returns true for canEdit')

  t.deepEqual(
    whakapapaView.tiaki,
    [
      {
        id: publicProfileId,
        preferredName: null
      }
    ],
    'returns correct tiaki'
  )

  t.deepEqual(
    whakapapaView.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors
  const saveRes = await save(
    {
      id: whakapapaId,
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      }
    }
  )

  t.error(saveRes.errors, 'updates whakapapa without errors')

  const updatedWhakapapaRes = await get(whakapapaId)
  t.error(updatedWhakapapaRes.errors, 'gets updated whakapapa without error')

  const _whakapapaView = updatedWhakapapaRes.data.whakapapaView
  t.true(_whakapapaView.canEdit, 'can still edit because *')

  // HACK: cant test dates?
  const { start } = _whakapapaView.authors[1].intervals[0]

  t.deepEqual(
    _whakapapaView.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId again
  await save(
    {
      id: whakapapaId,
      authors: {
        remove: [feedId] // the feedId should be ignored because its already removed
      },
      recordCount: 5
    }
  )

  const deletedAuthorRes = await get(whakapapaId)
  t.error(deletedAuthorRes.errors, 'deletes authors without error')
  const whakapapa2 = deletedAuthorRes.data.whakapapaView

  // HACK: cant test dates?
  const { end } = whakapapa2.authors[1].intervals[0]

  t.true(whakapapa2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    whakapapa2.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})
