const test = require('tape')
const TestBot = require('../../test-bot')

function Save (apollo) {
  return async function save (input) {
    return apollo.mutate({
      mutation: `mutation($input: StoryInput!) {
        saveStory(input: $input)
      }`,
      variables: { input }
    })
  }
}

function Get (apollo) {
  return async function get (id) {
    return apollo.query({
      query: `query($id: ID!) {
        story(id: $id) {
          canEdit
          tiaki {
            id
            preferredName
          }
          authors {
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
            }
          }
        }
      }`,
      variables: {
        id
      }
    })
  }
}

test('story - authors', async t => {
  t.plan(13)

  const { ssb, apollo } = await TestBot({ loadContext: true, recpsGuard: true })

  const save = Save(apollo)
  const get = Get(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const saveStoryRes = await save(
    {
      type: 'test', // TODO: fails when '*' is used for the type
      authors: {
        add: [feedId],
        remove: []
      },
      recps: [feedId]
    }
  )

  t.error(saveStoryRes.errors, 'creates story without errors')
  const storyId = saveStoryRes.data.saveStory

  const storyRes = await get(storyId)
  t.error(storyRes.errors, 'gets story without errors')

  const story = storyRes.data.story
  t.true(story.canEdit, 'returns true for canEdit')

  t.deepEqual(
    story.tiaki,
    [
      {
        id: publicProfileId,
        preferredName: null
      }
    ],
    'returns correct tiaki'
  )

  t.deepEqual(
    story.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors
  const saveRes = await save(
    {
      id: storyId,
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      }
    }
  )

  t.error(saveRes.errors, 'updates story without errors')

  const updatedStoryRes = await get(storyId)
  t.error(updatedStoryRes.errors, 'gets updated profile without error')

  const _story = updatedStoryRes.data.story
  t.true(_story.canEdit, 'can still edit because *')

  // HACK: cant test dates?
  const { start } = _story.authors[1].intervals[0]

  t.deepEqual(
    _story.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId again
  await save(
    {
      id: storyId,
      authors: {
        remove: [feedId] // the feedId should be ignored because its already removed
      }
    }
  )

  const deletedAuthorRes = await get(storyId)
  t.error(deletedAuthorRes.errors, 'deletes authors without error')
  const story2 = deletedAuthorRes.data.story

  // HACK: cant test dates?
  const { end } = story2.authors[1].intervals[0]

  t.true(story2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    story2.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})
