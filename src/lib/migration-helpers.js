const fs = require('fs')
const path = require('path')
const { promisify: p } = require('util')

const migrationTemplate =
`module.exports = {
  up (ssb, misc, cb) {
    cb(null) // dont forget to callback!
  }
}
`

const testTemplate = (filename) =>
`const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')
const migration = require('../../src/migrations/${filename}')

test('migration/${filename}', async t => {
  const { ssb } = await TestBot()

  t.fail('Please add tests for migrations/${filename}')
  await p(migration.up)(ssb, {})

  ssb.close()
  t.end()
})
`

async function generateMigration (filename) {
  const migrationFilename = `${Date.now()}-${filename}`

  await p(writeMigrationFile)(migrationFilename)
  await p(writeMigrationTestFile)(migrationFilename)

  console.log('files created:')
  console.log(`- src/migrations/${migrationFilename}.js`)
  console.log(`- test/migrations/${migrationFilename}.test.js`)
}

function writeMigrationFile (filename, cb) {
  const fullFilename = `${filename}.js`
  fs.writeFile(
    path.join(__dirname, '../migrations', fullFilename),
    migrationTemplate,
    'utf8',
    (err) => {
      if (err) return cb(err)
      cb(null, fullFilename)
    }
  )
}

function writeMigrationTestFile (filename, cb) {
  const fullFilename = `${filename}.test.js`

  fs.writeFile(
    path.join(__dirname, '../../test/migrations', fullFilename),
    testTemplate(filename),
    'utf8',
    (err) => {
      if (err) return cb(err)
      cb(null, fullFilename)
    }
  )
}

module.exports = {
  generateMigration,
  migrationTemplate,
  testTemplate,
  writeMigrationFile,
  writeMigrationTestFile
}
