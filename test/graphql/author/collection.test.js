const test = require('tape')
const TestBot = require('../../test-bot')

function Save (apollo) {
  return async function save (input) {
    return apollo.mutate({
      mutation: `mutation($input: CollectionInput!) {
        saveCollection(input: $input)
      }`,
      variables: { input }
    })
  }
}

function Get (apollo) {
  return async function get (id) {
    return apollo.query({
      query: `query($id: ID!) {
        collection(id: $id) {
          canEdit
          tiaki {
            id
            preferredName
          }
          authors {
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
            }
          }
        }
      }`,
      variables: {
        id
      }
    })
  }
}

test('collection - authors', async t => {
  t.plan(13)

  const { ssb, apollo } = await TestBot({ recpsGuard: true })

  const save = Save(apollo)
  const get = Get(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const saveCollectionRes = await save(
    {
      type: 'test', // TODO: fails when '*' is used for the type
      authors: {
        add: [feedId],
        remove: []
      },
      recps: [feedId]
    }
  )

  t.error(saveCollectionRes.errors, 'creates collection without errors')
  const collectionId = saveCollectionRes.data.saveCollection

  const collectionRes = await get(collectionId)
  t.error(collectionRes.errors, 'gets collection without errors')

  const collection = collectionRes.data.collection
  t.true(collection.canEdit, 'returns true for canEdit')

  t.deepEqual(
    collection.tiaki,
    [
      {
        id: publicProfileId,
        preferredName: null
      }
    ],
    'returns correct tiaki'
  )

  t.deepEqual(
    collection.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors
  const saveRes = await save(
    {
      id: collectionId,
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      }
    }
  )

  t.error(saveRes.errors, 'updates collection without errors')

  const updatedCollectionRes = await get(collectionId)
  t.error(updatedCollectionRes.errors, 'gets updated collection without error')

  const _collection = updatedCollectionRes.data.collection
  t.true(_collection.canEdit, 'can still edit because *')

  // HACK: cant test dates?
  const { start } = _collection.authors[1].intervals[0]

  t.deepEqual(
    _collection.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId again
  await save(
    {
      id: collectionId,
      authors: {
        remove: [feedId] // the feedId should be ignored because its already removed
      },
      recordCount: 5
    }
  )

  const deletedAuthorRes = await get(collectionId)
  t.error(deletedAuthorRes.errors, 'deletes authors without error')
  const collection2 = deletedAuthorRes.data.collection

  // HACK: cant test dates?
  const { end } = collection2.authors[1].intervals[0]

  t.true(collection2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    collection2.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})
