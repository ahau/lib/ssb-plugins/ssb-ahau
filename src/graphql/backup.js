const gql = require('graphql-tag')
const { join } = require('path')
const { readFile } = require('fs')
const { promisify: p } = require('util')

module.exports = (ssb) => {
  const { resolvers } = Resolvers(ssb)

  return {
    typeDefs,
    resolvers
  }
}

const typeDefs = gql`
extend type Query {
  backup: String
}
`

const backupMessage = `# WARNING: Never show this to anyone.
# WARNING: Never edit it or use it on multiple devices at once.
#
# This is your SECRET.
# With this key you can decrypt all of your private information,
# sign your messages so that your friends can verify that the messages came from you.
# If anyone learns your secret, they can use it to access your information and impersonate you.
#
# If you use this secret on more than one device you will create a fork and
# your friends will stop replicating your content.
#
#
`

function getSbotPath (ssb) {
  return ssb.config.path
}

async function getLatestSequence (ssb) {
  const latestSequence = await p(ssb.getVectorClock)()

  return latestSequence[ssb.id]
}

async function getKeys (ssb) {
  return ssb.config.keys
}

async function getOwnKeys (ssb) {
  const keyInfo = await p(ssb.tribes.ownKeys.list)()

  return keyInfo.map(info => {
    info.key = info.key.toString('base64')
    return info
  })
}

async function getConnFileContent (ssb) {
  const filePath = join(getSbotPath(ssb), 'conn.json')
  const fileContent = await p(readFile)(filePath, 'utf8')

  return JSON.parse(fileContent)
}

async function getBackupContent (ssb) {
  // Get infos
  const secret = await getKeys(ssb)
  const ownKeys = await getOwnKeys(ssb)
  const latestSequence = await getLatestSequence(ssb)
  const connContent = await getConnFileContent(ssb)

  return {
    version: 2.0,
    secret,
    ownKeys,
    patakaLocations: connContent,
    latestSequence
  }
}

function Resolvers (ssb) {
  const resolvers = {
    Query: {
      backup: async () => {
        const backupContent = await getBackupContent(ssb)

        return [
          backupMessage,
          JSON.stringify(backupContent)
        ].join('\n')
      }
    }
  }

  return {
    resolvers
  }
}
