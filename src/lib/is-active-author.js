module.exports = function isActiveAuthor (intervals, seq) {
  if (intervals == null || seq == null) return false

  return intervals.some(({ start, end }) => {
    return (
      start != null &&
      (seq >= start) &&
      (end === null || seq <= end)
    )
  })
}
