const pull = require('pull-stream')
const paraMap = require('pull-paramap')

// ensure all groups this user authored have a kaitiaki-only subgroup attached
module.exports = {
  up (ssb, misc, cb) {
    // load all groups you started (group/init)
    // see if they have a kaitiaki subgroup
    // if not, then give them one
    // done

    ssb.tribes.findByFeedId(ssb.id, (err, personalGroups) => {
      if (err) return cb(err)

      personalGroups = new Set(personalGroups.map(g => g.groupId))

      ssb.tribes.list((err, tribeIds) => {
        if (err) return cb(err)

        pull(
          pull.values(tribeIds),
          pull.filter(groupId => !personalGroups.has(groupId)),
          paraMap(ssb.tribes.get, 5),
          paraMap((data, cb) => {
            ssb.get(data.root, (err, val) => {
              if (err) return cb(err)

              cb(null, { author: val.author, groupId: data.groupId })
            })
          }, 5),
          pull.filter(msg => msg.author === ssb.id), // only check tribes you authored
          // check if the tribe has an admin group
          paraMap(({ groupId }, cb) => {
            ssb.tribes.findSubGroupLinks(groupId, (err, subGroupLinks) => {
              if (err) return cb(err)

              cb(null, { groupId, subGroupLinks })
            })
          }, 5),

          // we want to keep all groups that dont have an admin subgroup
          pull.filter(({ subGroupLinks }) => {
            if (!subGroupLinks || !subGroupLinks.length) return true

            return !subGroupLinks.some(link => link.admin)
          }),
          paraMap(({ groupId }, cb) => {
            ssb.tribes.subtribe.create(groupId, { admin: true, addPOBox: true }, cb)
          }, 5),
          pull.collect(cb)
        )
      })
    })
  }
}
