module.exports = function FindAdminSubGroupByGroupId (ssb) {
  return function findAdminSubGroupByGroupId (groupId, cb) {
    ssb.tribes.findSubGroupLinks(groupId, (err, subGroupLinks) => {
      if (err) return cb(err)

      const admin = subGroupLinks.filter(link => link.admin)

      if (admin.length === 0) return cb(new Error('found group without an admin subgroup'))

      cb(null, { groupId, adminSubGroupId: admin[0].subGroupId })
    })
  }
}
