const test = require('tape')
const fs = require('fs')
const path = require('path')
const pull = require('pull-stream')
const { promisify: p } = require('util')
const { writeMigrationTestFile } = require('../../src/lib/migration-helpers')

test('migrations', async t => {
  const migrationsPath = path.join(__dirname, '../../src/migrations')
  const migrationTestsPath = path.join(__dirname)

  const migrationFiles = await p(pullFileSet)(migrationsPath, -3)
  const migrationTestFiles = await p(pullFileSet)(migrationTestsPath, -8)
  const testFiles = new Set(migrationTestFiles)
  const neededFiles = new Set()

  pull(
    pull.values(migrationFiles),
    pull.filter(m => !testFiles.has(m)),
    pull.through((filename) => {
      t.fail(`no test found for migration ${filename}`)
      neededFiles.add(filename)
    }),
    pull.asyncMap(writeMigrationTestFile),
    pull.map(filename => filename.slice(0, -8)),
    pull.collect((err, newFileNames) => {
      if (err) throw err

      if (neededFiles.size !== newFileNames.length) {
        t.fail('the test files needed and the number of test files created were not the same!')
        t.end()
        return
      }

      if (neededFiles.length === 0 && newFileNames.length === 0) {
        t.pass('all migrations have test files')
        t.end()
        return
      }

      pullFileSet(migrationTestsPath, -8, (err, filenames) => {
        if (err) throw err

        const updatedTestFiles = new Set(filenames)

        t.true(
          newFileNames.every(filename => {
            return updatedTestFiles.has(filename) && neededFiles.has(filename)
          }),
          'new migrations were created for each of the files'
        )

        t.end()
      })
    })
  )
})

function pullFileSet (path, slice, cb) {
  fs.readdir(path, (err, filenames) => {
    if (err) throw err

    pull(
      pull.values(filenames),
      pull.filter(filename => filename !== 'migration.test.js'), // exclude this file
      pull.map(filename => filename.slice(0, slice)), // rm .js from the end
      pull.collect(cb)
    )
  })
}
