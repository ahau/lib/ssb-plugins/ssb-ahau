const rimraf = require('rimraf')
const fs = require('fs')

function unlink (path, cb) {
  fs.unlink(path, (err) => {
    if (err && err.message.match(/log.offset/)) {
      console.log('hiding fs.unlink error', err.message)
      return cb(null)
    }
    cb(err)
  })
}

function lstat (path, opts, cb) {
  if (typeof opts === 'function') {
    cb = opts
    opts = {}
  }

  fs.lstat(path, opts, (err, data) => {
    if (err && err.message.match(/log.offset/)) {
      console.log('hiding fs.lstat error', err.message)
      return cb(null, { isDirectory: () => false })
    }
    cb(err, data)
  })
}

function rmdir (path, opts, cb) {
  if (typeof opts === 'function') {
    cb = opts
    opts = {}
  }

  fs.rm(path, { recursive: true, force: true }, (err, data) => {
    if (err && err.message.match(/flume/)) {
      console.log('hiding fs.rm error', err.message)
      return cb(null)
    }
    cb(err, data)
  })
}

module.exports = function deleteAhau (ssb, cb) {
  ssb.close(() => {
    rimraf(ssb.config.path, { unlink, lstat, rmdir }, (err) => {
      if (err) console.error('rimraf error', err.message)

      cb(null, true)
    })
  })
}
